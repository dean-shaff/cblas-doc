doxygen:
	doxygen Doxyfile

html:
	poetry run sphinx-build -b html source dist
